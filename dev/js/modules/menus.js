export const mainmenu = [
  {
    title: 'Comunicación integral',
    href: 'comunicacion-integral',
    color: '--first',
    submenu: [
      {
        title: 'Estrategias y planes de comunicación',
        href: '#',
        state: true
      },
      {
        title: 'Diseño gráfico y producción de material impreso ',
        href: '#',
        state: false
      },
      {
        title: 'Producción audiovisual: videos, spots, documentales, jingles',
        href: '#',
        state: false
      },
      {
        title: 'Artículos publicitarios y material de merchandising.',
        href: '#',
        state: false
      },
    ]
  },
  {
    title: 'Organización de eventos corporativos',
    href: 'organizacion-de-eventos-corporativos',
    color: '--tertiary',
    submenu: [
      {
        title: 'Asesoramiento, diseño, planificación y producción de eventos empresariales',
        href: '#',
        state: true
      },
      {
        title: 'BTLs',
        href: '#',
        state: false
      },
      {
        title: 'Convenciones, seminarios, talleres, foros - debates y mesas redondas',
        href: '#',
        state: false
      },
      {
        title: 'Ferias, exposiciones y showrooms',
        href: '#',
        state: false
      },
      {
        title: 'Capacitación de personal y entrenamiento de equipos humanos',
        href: '#',
        state: false
      },
      {
        title: 'Organización de aniversarios, almuerzos, cenas y otros de acuerdo a requerimiento del cliente.',
        href: '#',
        state: false
      },
    ]
  },
  {
    title: 'Organización de espectáculos',
    href: 'organizacion-de-espectaculos',
    color: '--ghost--dark',
    submenu: [
      {
        title: 'Asesoramiento integral de eventos culturales ',
        href: '#',
        state: true
      },
      {
        title: 'Producción de espectáculos nacionales',
        href: '#',
        state: false
      },
      {
        title: 'Producción de shows artísticos internacionales',
        href: '#',
        state: false
      },
      {
        title: 'Locaciones: elección del lugar, luz, sonido, decoración y proceso ',
        href: '#',
        state: false
      },
      {
        title: 'Búsqueda de auspiciadores',
        href: '#',
        state: false
      },
      {
        title: 'Plan de medios y marketing general',
        href: '#',
        state: false
      },
      {
        title: 'Planificación de tours de medios, publicidad y promoción',
        href: '#',
        state: false
      },
      {
        title: 'Seguridad',
        href: '#',
        state: false
      },
      {
        title: 'Supervisión logística, técnica y de recursos humanos.',
        href: '#',
        state: false
      },
    ]
  }
];

export const menuinicio = [
  {
    title: 'Inicio',
    href: '',
  },
  {
    title: 'Nosotros',
    href: 'nosotros',
    active: false
  },
  {
    title: 'Contactos',
    href: 'contactos',
  }
];