//Slider principal
import { tns } from "./modules/tiny-slider"

//Animacion de click Scrooll
import SmoothScroll from "smooth-scroll/dist/js/smooth-scroll.min"

//WOW JS
import WOW from 'wow.js/dist/wow.min'
//Headroom JS
import Headroom from 'headroom.js/dist/headroom.min'
//BaguetteBox.js
import BaguetteBox from 'baguettebox.js/dist/baguetteBox.min'
//Modulo para reproducir videos de youtube
import MediaBox from './modules/mediabox'

//Data vuejs
import { contactform } from './modules/contactform'
import { menuinicio, mainmenu } from './modules/menus'

// import Vue from 'vue/dist/vue.min'
import Vue from 'vue/dist/vue'
import VueResource from 'vue-resource/dist/vue-resource.min'

Vue.use(VueResource);
// Vue.use(VanillaModal);

new Vue({
  el: '#comunicat',
  data: {
    showmodal: true,
    baseUrl: '/',
    toggle: false,
    formSubmitted: false,
    vue: contactform,
    menuinicio,
    mainmenu,
    isActive: true,
    error: null,
    nosotrosState: false,
    servicio: {
      proveedor: '',
      valor: ''
    }
  },
  created: () => {
    let wow = new WOW({
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 0,
      mobile: false,
      live: false
    })
    wow.init()
    var scroll = new SmoothScroll('a[href*="#"]', {
      // Selectors
      ignore: '[data-scroll-ignore]', // Selector for links to ignore (must be a valid CSS selector)
      header: null, // Selector for fixed headers (must be a valid CSS selector)
      
      // Speed & Easing
      speed: 500, // Integer. How fast to complete the scroll in milliseconds
      offset: 80, // Integer or Function returning an integer. How far to offset the scrolling anchor location in pixels
      easing: 'easeInOutCubic', // Easing pattern to use
      customEasing: function (time) { }, // Function. Custom easing pattern
      
      // Callback API
      before: function () { }, // Callback to run before scroll
      after: function () { } // Callback to run after scroll
    });
    
    //FOOTER AÑO
    function getDate() {
      var today = new Date()
      var year = today.getFullYear()
      document.getElementById('currentDate').innerHTML = year
    }
    getDate()
  },
  mounted: function () {
    var header = new Headroom(document.querySelector('#header'), {
      tolerance: 15,
      offset: 10,
      tolerance: {
        up: 5,
        down: 0
      },
      classes: {
        initial: 'header-initial', //Cuando el elemento se inicializa
        pinned: 'header-up', //Cuando se deplaza hacia arriba
        unpinned: 'header-down', //Cuando se deplaza hacia abajo
        top: 'header-top', //Cuando esta pegado arriba
        notTop: 'header-notop', //Cuando no esta pegado arriba
        bottom: 'header-bottom', //Cuando esta pegado abajo
        notBottom: 'header-nobottom'//Cuando no esta pegado abajo
      }
    })
    header.init()

    BaguetteBox.run('.gallery__content', {
      // Custom options
    });

    let slider = tns({
      container: '.slider-comunicat',
      controlsText: [
        '<',
        '>'
      ],
      autoplayText: ['▶', '❚❚'],
      autoplay: true,
      speed: 3500,
      autoplayTimeout: 9000,
      lazyload: true,
      // animateIn: 'flipInY',
      // animateOut: 'bounceOut'
    });
    let poppup = tns({
      container: '.my-slider',
      controlsText: [
        '<',
        '>'
      ],
      // autoplayText: ['▶', '❚❚'],
      autoplay: true,
      // autoplayDirection: 'backward',
      speed: 2000,
      autoplayTimeout: 7000,
      lazyload: true,
      // animateIn: 'flipInY',
      // animateOut: 'bounceOut'
    });
    MediaBox('.mediabox');
    // Abrir todos los acordeones scripts2.js
    // for (let j = 0; j < this.mainmenu.length; j++) {
    //   for (let i = 0; i < this.mainmenu[j].submenu.length; i++) {
    //     this.mainmenu[j].submenu[i].state = true;
    //   }
    // }
  },
  methods: {
    showModal: function exitModal() {
        this.showmodal = false        
    },
    isFormValid: function () {
      return this.nombre != ''
    },
    submitForm: function () {
      if (!this.isFormValid()) return
      this.formSubmitted = true
      this.$http.post('/mail.php', { vue: this.vue }).then(function (response) {
        this.vue.envio = response.data
        this.clearForm()
      }, function () { })
    },
    clearForm: function () {
      this.vue.nombre = ''
      this.vue.email = ''
      this.vue.telefono = ''
      this.vue.movil = ''
      this.vue.direccion = ''
      this.vue.ciudad = ''
      this.vue.mensaje = ''
      this.vue.formSubmitted = false
    },
    toggleMenu: function () {
      if (this.toggle == true) {
        this.toggle = false
        // console.log('DESACTIVADO')
      }
      else {
        this.toggle = true
        // console.log('ACTIVO')
      }
    },
    acordionVue: function (message, array) {
      if (this.mainmenu[array].submenu[message].state == true){
        this.mainmenu[array].submenu[message].state = false
      }else{
        for (let i = 0; i < this.mainmenu[array].submenu.length; i++){
          this.mainmenu[array].submenu[i].state = false;
        }
        this.mainmenu[array].submenu[message].state = true;
      }
    },
    mostrarVue: function (estado) {
      this.nosotrosState = estado;
    },
    idYoutube: function(url){
        var service = {},
        matches;
        if (matches = url.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/)) {
          service.provider = "youtube";
          service.id = matches[2];
        } else if (matches = url.match(/https?:\/\/(?:www\.)?vimeo.com\/(?:channels\/|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/)) {
          service.provider = "vimeo";
          service.id = matches[3];
        } else {
          service.provider = "Unknown";
          service.id = '';
        }
        this.servicio.valor = service.id;
        this.servicio.proveedor = service.provider;
        return service.id;
    }
  }
})